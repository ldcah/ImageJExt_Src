package com.ldcah;

import ij.ImagePlus;
import ij.io.OpenDialog;

import java.io.File;

public class ShowImageInfo {
    public static void main(String args[]) {

        String we = "";
        if (args != null && args.length == 1) {
            we = args[0];
        } else {

            OpenDialog wo = new OpenDialog("");
            we = wo.getPath();
        }

        if (we == null || we.equals("")) {
            System.exit(0);
        }
        File file = new File(we);
        if (!file.exists()) {
            System.exit(0);
        }

        ImagePlus ming = new ImagePlus(we);
        ming.setTitle("图像");
        ming.show();
        System.out.println("main finished");

    }
}
